import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/Profile.dart';
import 'package:flutter_application_1/Profile.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.@override
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      home: Profile(),
      theme: ThemeData(
          primarySwatch: Colors.amber,
          accentColor: Colors.amberAccent,
          scaffoldBackgroundColor: Colors.blueGrey),
    );
  }
}

Widget build(BuildContext context) {
  return MaterialApp(
    home: AnimatedSplashScreen(
      splash: Image.asset(
        'assets/AvatarMaker.png',
      ),
      nextScreen: Profile(),
      splashTransition: SplashTransition.fadeTransition,
      backgroundColor: Color.fromARGB(255, 13, 29, 56),
    ),
  );
}
